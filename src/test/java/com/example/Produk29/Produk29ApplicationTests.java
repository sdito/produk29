package com.example.Produk29;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class Produk29ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    /* test insert api */
    @Test
    public void testConnectionInsert() throws Exception {
        this.mockMvc.perform(get("/rest/insert?name=testName1&latitude=123456&longitude=123456")).andDo(print()).andExpect(status().isOk());
    }

    /* test get api */
    @Test
    public void testConnectionGet() throws Exception {
        this.mockMvc.perform(get("/rest/get")).andDo(print()).andExpect(status().isOk());
    }

}
