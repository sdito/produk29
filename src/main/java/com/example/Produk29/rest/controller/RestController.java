package com.example.Produk29.rest.controller;

import com.example.Produk29.model.PointOfInterest;
import com.example.Produk29.service.RestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/* Handle requests */
@org.springframework.web.bind.annotation.RestController
public class RestController {

    private RestService restService = new RestService();

    /* insert a point of interest in the database */
    @RequestMapping(value = "/rest/insert", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<HttpStatus>insertPoint(@RequestParam(value = "name", defaultValue = "Anonym") String name,
                       @RequestParam(value = "latitude") float latitude,
                       @RequestParam(value = "longitude") float longitude) {
        try {
            if(restService.insertPoint(name, latitude, longitude)) {
                return new ResponseEntity(HttpStatus.OK);
            } else return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /* return the list of points of interest */
    @GetMapping(value = "/rest/get")
    public @ResponseBody ResponseEntity<List<PointOfInterest>> getPoints() {
        return ResponseEntity.ok(restService.getPoints());
    }

}
