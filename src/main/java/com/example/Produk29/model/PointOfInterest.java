package com.example.Produk29.model;

public class PointOfInterest {

    private String name;
    private float latitude;
    private float longitude;

    public PointOfInterest(String name, float latitude, float longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public PointOfInterest(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

}
