package com.example.Produk29.service;

import com.example.Produk29.model.PointOfInterest;

import java.util.ArrayList;
import java.util.List;

/* Helper class which should be replaced with CRUD operations */
public class RestService {

    private List<PointOfInterest> db = new ArrayList<>(); //emulated database

    /* add a new POI to the database */
    public boolean insertPoint(String name, float latitude, float longitude) {
        return db.add(new PointOfInterest(name, latitude, longitude));
    }

    /* read the list of POI from the database */
    public List<PointOfInterest> getPoints() {
        return db;
    }

}
