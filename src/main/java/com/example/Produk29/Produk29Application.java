package com.example.Produk29;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Produk29Application {

	public static void main(String[] args) {
		SpringApplication.run(Produk29Application.class, args);
	}

}
