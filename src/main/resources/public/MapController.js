var MapApp = angular.module("MapApp", []);

MapApp.controller('MapController', function($scope, $http) {
    var mymap = L.map('mapid').setView([40.6194, 22.9454], 13);
    var markerGroup = L.layerGroup().addTo(mymap);

    /* initialize Map with OSM and Leaflet*/
    $scope.initMap = function() {

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mymap);

    }

    /* submit a new POI */
    $scope.submitPoint = function() {
        // POST
        return $http.get('http://localhost:8080/rest/insert?name=' + $scope.form.name + '&latitude=' + $scope.form.latitude + '&longitude=' + $scope.form.longitude).then(function(res) {
            $scope.getPoints();
        }, function(err) {
            console.log(err);
        });
    };

    /* get the list of POI */
    $scope.getPoints = function () {
        // GET
        return $http.get('http://localhost:8080/rest/get').then(function(res) {
            $scope.list = res.data;
        }, function(err) {
            console.log(err);
        });
    };

    /* add a marker to the map */
    $scope.addMarker = function(latitude, longitude, name) {
        var coordinate = [parseFloat(latitude), parseFloat(longitude)];
        L.marker(coordinate).addTo(markerGroup).bindPopup("<b>" + name + "</b></br>" + latitude + ", " + longitude).openPopup();
    }

});
